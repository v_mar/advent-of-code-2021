# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day9_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [parse.(Int64, collect(x)) for x in split(input, "\n")]
input = copy(transpose(reduce(hcat, input)))

# ==============================================================================
# algorithm
# ==============================================================================
function day9part1(input::Matrix{Int64})
    dij = [0 1 0 -1;
           -1 0 1 0]
           
    is, js = size(input)

    sum_of_risklevels = 0

    for i in 1:is, j in 1:js
        current = input[i, j]
        compare = 10

        for k = 1:4
            ii = i + dij[1, k]
            jj = j + dij[2, k]
            if (1 <= ii <= is) && (1 <= jj <= js)
                compare = min(compare, input[ii, jj])
            end
        end

        if current < compare
            sum_of_risklevels += (current + 1)
        end

    end
    return sum_of_risklevels
end

function day9part2(input::Matrix{Int64})
    dij = [0 1 0 -1;
          -1 0 1 0]

    is, js = size(input)

    matr = Array{Int64}(undef, (is, js))

    counter = 1
    for i in 1:is, j in 1:js
        if input[i, j] == 9
            matr[i, j] = 0
        else
            matr[i, j] = counter
            counter += 1
        end
    end

    changed = true
    while changed
        changed = false
        for i in 1:is, j in 1:js
            current = matr[i, j]

            if current == 0
                continue
            end

            for k = 1:4
                ii = i + dij[1, k]
                jj = j + dij[2, k]
                if (1 <= ii <= is) && (1 <= jj <= js)
                    surr = matr[ii, jj]
                    if surr != 0 && surr < current
                        current = surr
                        changed = true
                    end
                end
            end
            matr[i, j] = current
        end
    end

    basins = Dict{Int64,Int64}()
    for i in 1:is, j in 1:js
        if matr[i, j] in keys(basins)
            basins[matr[i, j]] += 1
        else
            basins[matr[i, j]] = 1
        end
    end

    basins = sort(collect(values(basins)), rev=true)

    return basins[2] * basins[3] * basins[4]

end


input_day9part1 = deepcopy(input)
input_day9part2 = deepcopy(input)

using BenchmarkTools
@btime day9part1($input_day9part1)
@btime day9part2($input_day9part2)

t1 = time()
num = day9part1(input_day9part1)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

t1 = time()
num = day9part2(input_day9part2)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")






