# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day4_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
bingo_cards = [reduce(hcat, [parse.(Int64, split(y)) for y in split(x, "\n")])' for x in split(input, "\n\n")[2:end]]
bingo_numbers = [parse(Int64, x) for x in split(split(input, "\n\n")[1], ",")]

# ==============================================================================
# part 1 and 2
# ==============================================================================
function part1and2(bingo_cards, bingo_numbers, part2=false)
    array_of_winners = Int64[]
    last_winner = 0

    bingo_cards_ = deepcopy(bingo_cards)

    num_rows, num_cols = size(bingo_cards_[1])

    for number in bingo_numbers
        for jj = 1:length(bingo_cards_)
            if !(jj in array_of_winners)
                card = bingo_cards_[jj]

                for i in 1:num_rows, j in 1:num_cols
                    if card[i, j] == number
                        card[i, j] = 0
                    end
                end

                bingo = false

                for j = 1:num_cols
                    temp_num = 0
                    for i = 1:num_rows
                        if card[i, j] != 0
                            break
                        else
                            temp_num += 1
                        end
                    end
                    
                    if temp_num == 5
                        bingo = true
                        break
                    end
                end

                if !bingo
                    for i = 1:num_rows
                        temp_num = 0
                        for j = 1:num_cols
                            if card[i, j] != 0
                                break
                            else
                                temp_num += 1
                            end
                        end
                        
                        if temp_num == 5
                            bingo = true
                            break
                        end
                    end
                end

                if bingo
                    if !part2
                        return sum(card) * number
                    else
                        push!(array_of_winners, jj)
                        last_winner = sum(card) * number
                    end
                end
            end
        end
    end
    return last_winner
end

function day4part1(bingo_cards, bingo_numbers)
    return part1and2(bingo_cards, bingo_numbers)
end

function day4part2(bingo_cards, bingo_numbers)
    return part1and2(bingo_cards, bingo_numbers, true)
end

t1 = time()
num = day4part1(bingo_cards, bingo_numbers)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day4part1($bingo_cards, $bingo_numbers)

t1 = time()
num = day4part2(bingo_cards, bingo_numbers)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day4part2($bingo_cards, $bingo_numbers)


# ==============================================================================
# potential
# ==============================================================================
# -> make one large array out of the cards

