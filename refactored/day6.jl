# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day6_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
vector = parse.(Int64, split(input, ","))

# ==============================================================================
# part 1 and 2
# ==============================================================================

function part1and2(vector, till_day)

    fish_vector = zeros(Int64, 9)
    for num in vector
        fish_vector[num + 1] += 1
    end

    # start simulation loop
    for day in 1:till_day
        fish_at_zero = fish_vector[1]
        fish_vector[1:8] = fish_vector[2:9]
        fish_vector[9] = fish_at_zero
        fish_vector[7] += fish_at_zero
    end
    return sum(fish_vector)
end

function day6part1(vector)
    return part1and2(vector, 80)
end

function day6part2(vector)
    return part1and2(vector, 256)
end


input_day6part1 = deepcopy(vector)
input_day6part2 = deepcopy(vector)

using BenchmarkTools
@btime day6part1($input_day6part1)
@btime day6part2($input_day6part2)


t1 = time()
num = day6part1(input_day6part1)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

t1 = time()
num = day6part2(input_day6part2)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")


# ==============================================================================
# optim
# ==============================================================================

