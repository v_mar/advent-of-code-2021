# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day1_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [parse(Int64, x) for x in split(input, "\n")]

# ==============================================================================
# part 1
# ==============================================================================
function day1part1(input)
    num = 0
    for i = 2:length(input)
        if input[i] > input[i - 1]
            num += 1
        end
    end
    return num
end

t1 = time()
num = day1part1(input)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day1part1(input)

# ==============================================================================
# part 2
# ==============================================================================
function day1part2(input)
    num = 0
    for i = 1:length(input) - 3
        sum1 = 0
        sum2 = 0

        sum1 += input[i]
        for ii = i + 1:i + 2
            sum1 += input[ii]
            sum2 += input[ii]
        end
        sum2 += input[i + 3]
        
        if sum2 > sum1
            num += 1
        end

    end
    return num
end

t1 = time()
num = day1part2(input)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day1part2(input)

