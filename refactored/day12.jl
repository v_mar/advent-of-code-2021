# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day12_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [split(x, "-") for x in split(input, "\n")]

# ==============================================================================
# build a path dictionary
# ==============================================================================
path_dict = Dict{String,Vector{String}}()

for row in input
    push!(get!(path_dict, row[1], []), row[2])
    push!(get!(path_dict, row[2], []), row[1])
end

# ==============================================================================
# find all paths - first part
# ==============================================================================
# ==============================================================================
# with primes
# ==============================================================================
lowercases = unique([i for a in input for i in a if islowercase(i[1]) && !(i in ["start", "end"])])

using Primes
primes = []
ii = 1
while length(primes) <= length(lowercases)
    if isprime(ii)
        push!(primes, ii)
    end
    ii += 1
end

primes_dict = Dict{String,Int64}()
for ii in 1:length(lowercases)
    primes_dict[lowercases[ii]] = primes[ii]
end

using Memoization
@memoize function day12part1and2(pos::String, current_path::Int64, 
                        path_dict::Dict{String,Vector{String}},
                        primes_dict::Dict{String,Int64},
                        joker::Bool)

    number_paths = 0

    for opt in path_dict[pos]
        if opt == "end"
            number_paths += 1

        elseif opt == "start"
            continue

        elseif islowercase(opt[1])
            if current_path % primes_dict[opt] != 0
                number_paths += day12part1and2(opt, current_path * primes_dict[opt], path_dict, primes_dict, joker)
            else
                if !joker
                    number_paths += day12part1and2(opt, current_path, path_dict, primes_dict, true)
                end
            end

        else
            number_paths += day12part1and2(opt, current_path, path_dict, primes_dict, joker)
        end
    end

    return number_paths
end

input_day12part1 = ["start", 1, path_dict, primes_dict, true]
input_day12part2 = ["start", 1, path_dict, primes_dict, false]

using BenchmarkTools
@btime day12part1and2(input_day12part1...) setup = (Memoization.empty_cache!(day12part1and2))
@btime day12part1and2(input_day12part2...) setup = (Memoization.empty_cache!(day12part1and2))
