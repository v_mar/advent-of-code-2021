using OffsetArrays

# ==============================================================================
# input
# ==============================================================================
# final input
f = open("./inputs/day21_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = split(input, "\n")
input = [parse(Int64, split(x, ": ")[2]) for x in input]
positions = deepcopy(input)

# ==============================================================================
# algorithm part 1
# ==============================================================================

function day21part1(positions)
    points = Int64[0, 0]

    # offset the vector to make the modulo work for wrapping around
    die_iter = OffsetVector(1:100, 0:99) 

    die = 0
    won = false
    while !won
        for player = 1:2
            die_val = 0
            for _ in 1:3
                die_val += die_iter[die % 100]
                die += 1
            end

            positions[player] += die_val > 10 ? die_val - 10 * trunc(Int64, die_val / 10) : die_val
            positions[player] = positions[player] > 10 ? positions[player] - 10 * trunc(Int64, positions[player] / 10) : positions[player]
            points[player] += positions[player]

            if maximum(points) > 999
                won = true
                break
            end      
        end
    end
    return minimum(points) * die
end

using BenchmarkTools

@btime day21part1(positions)


# ==============================================================================
# again other
# ==============================================================================
using DataStructures

function day21part2(init_pos1, init_pos2; WIN_SCORE=21)

    probabilities = ((3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1))
    states = [DefaultDict{Tuple{Int64,Int64},Int64}(0), DefaultDict{Tuple{Int64,Int64},Int64}(0)]

    states[1][(0, init_pos1)] = 1
    states[2][(0, init_pos2)] = 1

    r = 0
    other_ct = 1
    wins = [0, 0]

    while other_ct != 0
        new_state = DefaultDict{Tuple{Int64,Int64},Int64}(0)

        for (score, pos) in keys(states[r % 2 + 1])

            for (die, ct) in probabilities

                new_pos = (pos + die - 1) % 10 + 1
                new_score = score + new_pos

                if new_score < WIN_SCORE
                    new_state[(new_score, new_pos)] += ct * states[r % 2 + 1][(score, pos)]

                else
                    wins[r % 2 + 1] += ct * other_ct * states[r % 2 + 1][(score, pos)]
        
                end
            end
        end

        states[r % 2 + 1] = new_state
        r += 1
        other_ct = sum(values(states[(r + 1) % 2 + 1]))

    end
    return maximum(wins), states
end


using BenchmarkTools

@btime day21part2(positions...)

# ==============================================================================
# own solution -> recursion with cache -> 293.424 ms (6859237 allocations: 313.57 MiB)
# ==============================================================================
# using Memoization

# @memoize Dict function day21part2(pos1::Int64, pos2::Int64, poi1::Int64, poi2::Int64, won_at=21)

#     win1 = 0
#     win2 = 0

#     for die1_1 in 1:3, die1_2 in 1:3, die1_3 in 1:3
#             new_pos1 = (((pos1 + die1_1 + die1_2 + die1_3) - 1) % 10) + 1
#             new_poi1 = poi1 + new_pos1

#             if new_poi1 >= won_at
#                 win1 += 1
#                 continue
#             end
            
#         for die2_1 in 1:3, die2_2 in 1:3, die2_3 in 1:3
#             new_pos2 = (((pos2 + die2_1 + die2_2 + die2_3) - 1) % 10) + 1
#             new_poi2 = poi2 + new_pos2

#             if new_poi2 >= won_at
#                 win2 += 1
#                 continue
#             end

#             won_level_below = day21part2(new_pos1, new_pos2, new_poi1, new_poi2)
#             win1 += won_level_below[1]
#             win2 += won_level_below[2]
#         end
#     end
#     return win1, win2
# end

# t1 = time()
# win1, win2 = day21part2(10, 8, 0, 0)
# t2 = time()

# print(t2 - t1)

# @btime day21part2(10, 8, 0, 0) setup = (Memoization.empty_cache!(day21part2))

# # using Profile
# # using ProfileView

# # Profile.clear()
# # Memoization.empty_cache!(day21part2)
# # @profile day21part2(10, 8, 0, 0)
# # ProfileView.view()


