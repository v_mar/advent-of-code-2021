# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day11_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [parse.(Int64, collect(x)) for x in split(input, "\n")]
input = reduce(hcat, input)'

# ==============================================================================
# first algo for the second problem
# ==============================================================================

function day11part1and2(matr, steps=100, part2=false)
    dij = [0 1 1 1 0 -1 -1 -1;
          -1 -1 0 1 1 1 0 -1]

    is = size(matr)[1]
    js = size(matr)[2]

    flashed_already = zeros(Int8, size(matr))
    watr = copy(matr)

    step_number = 0
    flashes = 0

    while true
        step_number += 1
        flashed_already .= 0
        watr .+= 1
        
        changing = true
        while changing
            changing = false
            
            for i in 1:is, j in 1:js
                if watr[i, j] > 9
                    changing = true
                    watr[i, j] = 0
                    flashed_already[i, j] += 1
                    flashes += 1
                    for k = 1:8
                        ii = i + dij[1, k]
                        jj = j + dij[2, k]
                        if (1 <= ii <= is) && (1 <= jj <= js) && (flashed_already[ii, jj] == 0)
                            watr[ii, jj] += 1
                        end
                    end
                end           
            end 
        end

        if !part2 && step_number >= steps
            return step_number, flashes
        elseif part2 && !any(flashed_already .== 0)
            return step_number, flashes
        end

    end
end


day11part1(matr) = day11part1and2(matr, 100, false)
day11part2(matr) = day11part1and2(matr, 100, true)

input_day11part1 = deepcopy(copy(input))
input_day11part2 = deepcopy(copy(input))

using BenchmarkTools
@btime day11part1($input_day11part1)
@btime day11part2($input_day11part2)





