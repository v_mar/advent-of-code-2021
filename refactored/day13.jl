# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day13_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
numbers, folding = split(input, "\n\n")
numbers = [tuple(parse.(Int64, split(x, ","))...) for x in split(numbers, "\n")]
last_iteration = numbers

folding = [split(x, "=") for x in split(folding, "\n")]
folding = [[fol[1][end], parse(Int64, fol[2])] for fol in folding]

# ==============================================================================
# algorithm
# ==============================================================================

function day13part1and2(last_iteration::Vector{Tuple{Int64,Int64}}, folding::Vector{Vector{Any}}, part2::Bool)
    next_iteration = Set{Tuple{Int64,Int64}}()
    for fold in folding
        if fold[1] == 'x'
            for point in last_iteration
                dist = point[1] - fold[2]

                if dist > 0
                    push!(next_iteration, (fold[2] - dist, point[2]))
                else
                    push!(next_iteration, (point[1], point[2]))
                end
            end
        else
            for point in last_iteration
                dist = point[2] - fold[2]

                if dist > 0
                    push!(next_iteration, (point[1], fold[2] - dist))
                else
                    push!(next_iteration, (point[1], point[2]))
                end
            end
        end

        if !part2
            return next_iteration
        end

        last_iteration = copy(next_iteration)
        empty!(next_iteration)
    end
    return last_iteration
end

input_day13part1 = [last_iteration, folding, false]
input_day13part2 = [last_iteration, folding, true]

using BenchmarkTools
@btime day13part1and2(input_day13part1...)
@btime day13part1and2(input_day13part2...)

