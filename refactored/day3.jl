# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day3_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [parse.(Int16, collect(row)) for row in split(input, "\n")]
input = reduce(hcat, input)'

# ==============================================================================
# part 1
# ==============================================================================
function bin_int(x)
    num = 0
    for (pow, i) in enumerate(x[end:-1:1])
        num += i * 2^(pow - 1)
    end
    return num
end

function day3part1(input)
    binary_gamma_rate = sum(input, dims=1) .> size(input)[1] / 2
    binary_epsilon_rate = binary_gamma_rate .!= 1
    return bin_int(binary_gamma_rate) * bin_int(binary_epsilon_rate)
end

t1 = time()
num = day3part1(input)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day3part1($input)


# ==============================================================================
# part 2
# ==============================================================================

function day3part2(input)

    ox_gen_rat = input

    for bit_pos = 1:size(ox_gen_rat)[2]
        ox_gen_rat = ox_gen_rat[ox_gen_rat[:, bit_pos] .== (sum(ox_gen_rat[:, bit_pos]) / size(ox_gen_rat)[1] .>= 0.5 ? 1 : 0), :]
        if size(ox_gen_rat)[1] == 1
            break
        end
    end
    
    co2_scr_rat = input
    
    for bit_pos = 1:size(co2_scr_rat)[2]
        co2_scr_rat = co2_scr_rat[co2_scr_rat[:, bit_pos] .!= (sum(co2_scr_rat[:, bit_pos]) / size(co2_scr_rat)[1] >= 0.5 ? 1 : 0), :]
        if size(co2_scr_rat)[1] == 1
            break
        end
    end

return bin_int(ox_gen_rat) * bin_int(co2_scr_rat)
end

t1 = time()
num = day3part2(input)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day3part2($input)


