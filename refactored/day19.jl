# ==============================================================================
# input 
# ==============================================================================
# 3D input
f = open("./inputs/day19_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse
# ==============================================================================
# parse 
input = [split(x, "\n")[2:end] for x in split(input, "\n\n")]
input_ = [[tuple(parse.(Int64, split(y, ","))...) for y in z] for z in input]
scanners = deepcopy(input_)

# ==============================================================================
# find the overlapping scanners
# ==============================================================================
function squared_dist(point1, point2)
    return sum((point2 .- point1).^2)
end

function dist_matrix(scanners)
    dist_matrices = Matrix{Int64}[]
    for scanner in scanners
        dists = zeros(Int64, length(scanner), length(scanner))
        for i in 1:length(scanner), j in i + 1:length(scanner)
            dists[i, j] = squared_dist(scanner[i], scanner[j])
        end
        push!(dist_matrices, dists)
    end
    return dist_matrices
end

function overlapping_scanners(dist_matrices)
    overlapping = Dict{Tuple{Int64,Int64},typeof((CartesianIndex(0, 0), CartesianIndex(0, 0)))}()
    dist_mat_sets = [Set(d) for d in dist_matrices]
    for i in 1:length(dist_matrices), j in i + 1:length(dist_matrices)
        inters = intersect(dist_mat_sets[i], dist_mat_sets[j])
        if length(inters) >= 66
            dist = pop!(inters)
            ind_first = findfirst(isequal(dist), dist_matrices[i])
            ind_second = findfirst(isequal(dist), dist_matrices[j])
            overlapping[(i, j)] = (ind_first, ind_second)
            overlapping[(j, i)] = (ind_second, ind_first)
        end
    end
    return overlapping
end


# ==============================================================================
# find orientations
# ==============================================================================
function rotate90left(points, ax)
    if ax == 1
        return [(x, -z, y) for (x, y, z) in points]
    elseif ax == 2
        return [(z, y, -x) for (x, y, z) in points]
    elseif ax == 3
        return [(-y, x, z) for (x, y, z) in points]
    end
end

function reorient_points(points, instructions)
    ax1, rot1, ax2, rot2 = instructions

    angle1 = 0
    while angle1 < rot1
        points = rotate90left(points, ax1)
        angle1 += 90
    end

    angle2 = 0
    while angle2 < rot2
        points = rotate90left(points, ax2)
        angle2 += 90
    end

    return points
end

function find_least_expansive_orientations()
    list = []
    aa = collect(0:90:270)
    for i in 1:length(aa), j in 1:length(aa)
        push!(list, (aa[i], aa[j]))
    end

    list = sort(by=x -> sum(x), list)

    points1 = scanners[1]
    ppp = []
    iii = []

    for (rot1, rot2) in list
        for ax1 = 1:3
            for ax2 = 1:3
                instr = (ax1, rot1, ax2, rot2)
                points = reorient_points(points1, instr)
                if !(points in ppp)
                    push!(ppp, points)
                    push!(iii, instr)
                end
            end
        end
    end

    return iii
end


function get_relative_transformations(scanners, overlaps, unique_orientations; radius=1000)
    orient_dict = Dict{Tuple{Int64,Int64},Tuple{Tuple{Int64,Int64,Int64},NTuple{4,Int64}}}()

    # iterate over all the scanner combinations
    for cur_comb in keys(overlaps)
        for rev = [true, false]
            
            points_1 = scanners[cur_comb[1]]
            points_2 = scanners[cur_comb[2]]

            which_points = overlaps[cur_comb]

            p1_1 = points_1[which_points[1][1]]
            p1_2 = points_1[which_points[1][2]]

            # iterate over all orientations
            for orient in unique_orientations
                # orient = unique_orientations[4]
                points_2_curr = reorient_points(points_2, orient)
                
                p2_1 = points_2_curr[which_points[2][1]]
                p2_2 = points_2_curr[which_points[2][2]]

                dist_1_1 = p1_1 .- p2_1 
                dist_2_1 = p1_2 .- p2_2

                dist_1_2 = p1_2 .- p2_1 
                dist_2_2 = p1_1 .- p2_2

                # println(dist_1 .- dist_2)

                if dist_1_1 == dist_2_1 
                    orient_dict[cur_comb] = (dist_1_1, orient)
                    break
                elseif dist_1_2 == dist_2_2
                    orient_dict[cur_comb] = (dist_1_2, orient)
                    break
                end 
            end
        end
    end
    return orient_dict
end



# ==============================================================================
# path finder
# ==============================================================================
function find_path(path, path_trans, all_paths, orient_dict)
    if length(all_paths) == 0
        if (path[end] == 1)
            all_paths = path_trans
        else
            for possible in keys(orient_dict)
                st = possible[1]
                en = possible[2]

                if en == path[end] && !(st in path)
                    path_ = push!(copy(path), st)
                    path_trans_ = push!(copy(path_trans), orient_dict[possible])
                    all_paths = find_path(path_, path_trans_, all_paths, orient_dict)                
                end
            end
        end
    end
    return all_paths
end

function find_all_transformations(scanners, orient_dict)
    transf_dict = Dict()

    for sc = 1:length(scanners)
        transf_dict[sc] = find_path([sc], [], [], orient_dict)
    end
    return transf_dict
end

# ==============================================================================
# transform the points scanner by scanner and put them into one list
# ==============================================================================
function transform_points(toints, offset, rotation)
# modify the second cloud
    ax1, rot1, ax2, rot2 = rotation
    angle1 = 0
    while angle1 < rot1
        toints = rotate90left(toints, ax1)
        angle1 += 90
    end
    angle2 = 0
    while angle2 < rot2
        toints = rotate90left(toints, ax2)
        angle2 += 90
    end
    toints = [(t .+ offset) for t in toints]
    return toints
end

function number_of_beacons(scanners, transf_instruct_dict)
    all_points  = []

    for sc = 1:length(scanners)
        trans_points = deepcopy(scanners[sc])
        for inst in transf_instruct_dict[sc]            
            trans_points = transform_points(trans_points, inst[1], inst[2])
        end
        append!(all_points, trans_points)
        length(unique(all_points))
    end
    return length(unique(all_points))
end

function manh_dist(point1, point2)
    return sum(abs.(point2 .- point1))
end


function day19part1(scanners, unique_orientations)
    dist_matrices = dist_matrix(scanners)
    overlaps = overlapping_scanners(dist_matrices)
    orient_dict = get_relative_transformations(scanners, overlaps, unique_orientations)
    transf_instruct_dict = find_all_transformations(scanners, orient_dict)
    number_ = number_of_beacons(scanners, transf_instruct_dict)
    return number_, transf_instruct_dict
end

function day19part2(transf_instruct_dict)
    scanner_pos = Array{Vector{Tuple{Int64,Int64,Int64}}}(undef, 36)
    fill!(scanner_pos, [(0, 0, 0)])

    for sc = 1:length(transf_instruct_dict)
        for inst in transf_instruct_dict[sc]
            scanner_pos[sc] = transform_points(scanner_pos[sc], inst[1], inst[2])
        end
    end

    maxx = 0
    for sc1 in 1:length(scanner_pos) - 1, sc2 in sc1 + 1:length(scanner_pos)
        maxx = max(maxx, manh_dist(scanner_pos[sc1][1], scanner_pos[sc2][1]))
    end

    return maxx
end

unique_orientations = find_least_expansive_orientations()

number_, transf_instruct_dict = day19part1(scanners, unique_orientations)
max_dist = day19part2(transf_instruct_dict)

using BenchmarkTools
@btime day19part1(scanners, unique_orientations)
@btime day19part2(transf_instruct_dict)

