using Base:Int64
# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day22_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
struct Cube
    on::Bool
    min_x::Int64; max_x::Int64
    min_y::Int64; max_y::Int64
    min_z::Int64; max_z::Int64
end

function parse_input(input::String)
    r = r"(on|off)\s*x=(-?\d+)\.\.(-?\d+)\s*,\s*y=(-?\d+)\.\.(-?\d+)\s*,\s*z=(-?\d+)\.\.(-?\d+)"
    cubes = Cube[]
    for line in split(rstrip(input), "\n")
        m = match(r, line)
        on = (m.captures[1] == "on" ? true : false)
        push!(cubes, Cube(on, parse.(Int, m.captures[2:7])...)) 
    end
    return cubes
end

input_day22part2 = parse_input(input)
input_day22part1 = [x for x in input_day22part2 if all(abs.([x.min_x, x.min_y, x.min_z, x.max_x, x.max_y, x.max_z]) .< 50)]

# ==============================================================================
# algorithm
# ==============================================================================

function get_volume(cube::Cube)
    return (cube.max_x - cube.min_x + 1) * (cube.max_y - cube.min_y + 1) * (cube.max_z - cube.min_z + 1)
end

function cube_intersect(cube_a::Cube, cube_b::Cube)
    min_x = max(cube_a.min_x, cube_b.min_x)
    max_x = min(cube_a.max_x, cube_b.max_x)
    min_y = max(cube_a.min_y, cube_b.min_y)
    max_y = min(cube_a.max_y, cube_b.max_y)
    min_z = max(cube_a.min_z, cube_b.min_z)
    max_z = min(cube_a.max_z, cube_b.max_z)

    if min_x > max_x || min_y > max_y || min_z > max_z
        return 0
    else
        return Cube(true, min_x, max_x, min_y, max_y, min_z, max_z)
end
end

function clip_all(clip_box, boxen)
    return collect(Set([clipped for clipped in (cube_intersect(box, clip_box) for box in boxen) if clipped != 0]))
end

function sum_volume(boxen)
    if length(boxen) == 0
        return 0
    end

    if length(boxen) > 1
        first = boxen[1]
        remainder = boxen[2:end]
        overlaps = clip_all(first, remainder)
        return get_volume(first) + sum_volume(remainder) - sum_volume(overlaps)
    else
        return get_volume(boxen[1])
end
end

function day22part1and2(cubes::Vector{Cube})
    if length(cubes) == 0 
        return 0
    end

    cur_cube = cubes[1]

    if !cur_cube.on
        return day22part1and2(cubes[2:end])
    end

    overlaps = clip_all(cur_cube, cubes[2:end])

    return get_volume(cur_cube) + day22part1and2(cubes[2:end]) - sum_volume(overlaps)
end


res = day22part1and2(input_day22part1)
res = day22part1and2(input_day22part2)

using BenchmarkTools

@btime day22part1and2($input_day22part1)
@btime day22part1and2($input_day22part2)


# using Profile
# using ProfileView

# Profile.clear()
# @profile day22part1and2(input_day22part1)
# ProfileView.view()
