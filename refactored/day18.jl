using Base:Int64
# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day18_input.txt")
input = read(f, String)
close(f)

input = split(input, "\n")

# ==============================================================================
# flatten the list
# ==============================================================================
function flatten_input(str::SubString{String})
    flattend_list = Int64[]
    levels = Int64[]

    cur_lev = 0
    for char in str
        if char == '['
            cur_lev += 1
        elseif char == ']'
            cur_lev -= 1
        elseif isnumeric(char)
            push!(flattend_list, parse(Int64, char))
            push!(levels, cur_lev)
        end
    end
    return flattend_list, levels
end

# ==============================================================================
# finish preparing the input
# ==============================================================================
input_day18 = Tuple{Vector{Int64},Vector{Int64}}[]

for inn in input
    push!(input_day18, flatten_input(inn))
end

# ==============================================================================
# algorithm
# ==============================================================================  

function sum_n_reduce(nums::Vector{Int64}, levs::Vector{Int64})
    changing = true
    while changing
        changing = false
        
        for i = 1:length(nums)
            if levs[i] > 4
                if (i > 1) 
                    nums[i - 1] += nums[i]
                end

                if (i + 2) <= length(nums) 
                    nums[i + 2] += nums[i + 1]
                end

                nums[i] = 0
                levs[i] -= 1 

                deleteat!(nums, i + 1)
                deleteat!(levs, i + 1)

                changing = true
                break
            end
        end

        if !changing
            for i = 1:length(nums)
                val = nums[i]
                if val > 9

                    nums[i] = trunc(Int64, val / 2)
                    insert!(nums, i + 1, ceil(Int64, val / 2))

                    levs[i] += 1
                    insert!(levs, i + 1, levs[i])

                    changing = true
                    break
                end 
            end 
        end 
    end
    return nums, levs
end

function magnitude(nums::Vector{Int64}, levs::Vector{Int64})
    while length(nums) > 1
        maxx = argmax(levs)
        nums[maxx] = 3 * nums[maxx] + 2 * nums[maxx + 1]
        levs[maxx] -= 1
        deleteat!(nums, maxx + 1)
        deleteat!(levs, maxx + 1)
    end
    return nums[1]
end

function day18part1(snail_numbers::Vector{Tuple{Vector{Int64},Vector{Int64}}})
    nums_levs = snail_numbers[1]

    for step_no = 2:length(snail_numbers)
        nums_levs_next = snail_numbers[step_no]

        nums = vcat(nums_levs[1], nums_levs_next[1])
        levs = vcat(nums_levs[2], nums_levs_next[2]) .+ 1

        nums_levs = sum_n_reduce(nums, levs)
    end

    return magnitude(deepcopy(nums_levs)...)
end

function day18part2(snail_numbers::Vector{Tuple{Vector{Int64},Vector{Int64}}})
    
    maxx = 0
    for i in 1:length(snail_numbers), j in 1:length(snail_numbers)
        i == j && continue

        nums_levs_one = snail_numbers[i]
        nums_levs_two = snail_numbers[j]

        nums = vcat(nums_levs_one[1], nums_levs_two[1])
        levs = vcat(nums_levs_one[2], nums_levs_two[2]) .+ 1

        snail_num = sum_n_reduce(nums, levs)
        mag = magnitude(deepcopy(snail_num)...)

        maxx = max(maxx, mag)
    end
    return maxx
end

day18part1(input_day18)
day18part2(input_day18)

using BenchmarkTools
@btime day18part1(input_day18)
@btime day18part2(input_day18)

# using Profile
# using ProfileView

# Profile.clear()
# @profile day18part2(input_day18)
# ProfileView.view()

