# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day16_input.txt")
input = read(f, String)
close(f)

# hex bin
hex_bin_input = """0 = 0000
1 = 0001
2 = 0010
3 = 0011
4 = 0100
5 = 0101
6 = 0110
7 = 0111
8 = 1000
9 = 1001
A = 1010
B = 1011
C = 1100
D = 1101
E = 1110
F = 1111"""

# ==============================================================================
# parse input
# ==============================================================================
hex_bin_input = [split(x, " = ") for x in split(hex_bin_input, "\n")]

# hex to binary dictionary
hex_bin = Dict()

for (x, y) in hex_bin_input 
    hex_bin[x] = y 
end

# ==============================================================================
# convert hex to binary
# ==============================================================================
new_string = []
for char in input
    push!(new_string, hex_bin[string(char)])
end

input_day16part1 = join(new_string)

# ==============================================================================
# helper functions
# ==============================================================================
function str_bin_int(x)
    num = 0
    for (pow, i) in enumerate(x[end:-1:1])
        num += parse(Int64, i) * 2^(pow - 1)
    end
    return num
end

look(str, x) = str_bin_int(str[x])

# ==============================================================================
# parsing the hierarchy
# ==============================================================================

function day16part1(str, ind, end_ind, num_packages, sum_vers_num)

    packages = []

    while (ind < end_ind) && (length(packages) < num_packages) && !(['0'] == unique(str[ind:end_ind]))

        packet_version = look(str, ind:ind + 2)
        packet_typeID = look(str, ind + 3:ind + 5)

        sum_vers_num += packet_version

        if packet_typeID == 4
            fiver_bits = []

            fiver_ind = ind + 6
            fiver_start =  parse(Int64, str[fiver_ind])

            while parse(Int64, str[fiver_ind]) != 0
                push!(fiver_bits, str[fiver_ind + 1:fiver_ind + 4])
                fiver_ind += 5
            end

            # one more time after the starting bit is 0
            push!(fiver_bits, str[fiver_ind + 1:fiver_ind + 4])
            fiver_bits = str_bin_int(join(fiver_bits))
            ind = fiver_ind + 5

            push!(packages, fiver_bits)

        else
            length_typeID = parse(Int64, str[ind + 6])

            if length_typeID == 0 # number of bit for all subpackets
                len_of_subpack = look(str, ind + 7:ind + 21) # -> length 15
                ind += 22

                # one level down
                ind, subpackages, sum_vers_num = day16part1(str, ind, ind + len_of_subpack, Inf, sum_vers_num)

            elseif length_typeID == 1 # number of subpackets
                num_of_subpack = look(str, ind + 7:ind + 17) # -> length 11
                ind += 18

                # one level down
                ind, subpackages, sum_vers_num = day16part1(str, ind, length(str), num_of_subpack, sum_vers_num)

            end

            # add the sub-packages to the package
            cur_pack = Dict()
            cur_pack["type"] = packet_typeID
            cur_pack["valu"] = subpackages
            push!(packages, copy(cur_pack))
        end
    end
    return ind, packages, sum_vers_num
end

# ==============================================================================
# part 2
# ==============================================================================
function day16part2(innn, operation=-1)
    coll = []
    for i = 1:length(innn) 
        if typeof(innn[i]) <: Dict
            value = day16part2(innn[i]["valu"], innn[i]["type"])
            push!(coll, value)
        else
            push!(coll, innn[i])
        end
    end

    if operation == 0           result = reduce(+, coll)
    elseif operation == 1       result = reduce(*, coll)
    elseif operation == 2       result = minimum(coll)
    elseif operation == 3       result = maximum(coll)
    elseif operation == 5       result = coll[1] > coll[2] ? 1 : 0
    elseif operation == 6       result = coll[2] > coll[1] ? 1 : 0
    elseif operation == 7       result = coll[2] == coll[1] ? 1 : 0
    else                        result = coll[1]
    end

    return result
end


using BenchmarkTools

ind, input_day16part2, sum_vers_num = day16part1(input_day16part1, 1, length(input_day16part1), Inf, 0)
@btime day16part1(input_day16part1, 1, length(input_day16part1), Inf, 0)
@btime day16part2(input_day16part2)


