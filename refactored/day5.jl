# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day5_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
put = [[parse.(Int32, split(elem, ",")) for elem in split(row, " -> ")] for row in split(input, "\n")]
array = [[x[1][1], x[1][2], x[2][1], x[2][2]] for x in put]
array = reduce(hcat, array)'

# filter the diagonal ones
array_vert_hori = array[((array[:, 1] .== array[:, 3]) .| (array[:, 2] .== array[:, 4])), :]

# ==============================================================================
# part 1 and 2
# ==============================================================================
# function part1and2(array_search)
#     # find all points where lines are
#     points = Dict{Tuple{Int32,Int32},Int8}()

#     # sizehint!(points, size(array_search)[1])

#     @inbounds for i = 1:size(array_search)[1]
#         Ax1 = array_search[i, 1]
#         Ay1 = array_search[i, 2]
#         Ax2 = array_search[i, 3]
#         Ay2 = array_search[i, 4]

#         xxx = collect(Ax1:(Ax1 < Ax2 ? 1 : -1):Ax2)
#         yyy = collect(Ay1:(Ay1 < Ay2 ? 1 : -1):Ay2)

#         for ii = 1:max(length(xxx), length(yyy))
#             p = (xxx[min(ii, length(xxx))], yyy[min(ii, length(yyy))])
#             points[p] = get(points, p, 0) + 1 
#         end
#     end

#     return count(i -> i > 1, values(points))
# end

function part1and2(array_search)
    # find all points where lines are
    points = zeros(Int8, (maximum(array_search), maximum(array_search)))

    @inbounds for i = 1:size(array_search)[1]
        Ax1 = array_search[i, 1]
        Ay1 = array_search[i, 2]
        Ax2 = array_search[i, 3]
        Ay2 = array_search[i, 4]

        xxx = collect(Ax1:(Ax1 < Ax2 ? 1 : -1):Ax2)
        yyy = collect(Ay1:(Ay1 < Ay2 ? 1 : -1):Ay2)

        for ii = 1:max(length(xxx), length(yyy))
            p = (xxx[min(ii, length(xxx))], yyy[min(ii, length(yyy))])
            points[p[1], p[2]] += 1 
        end
    end

    return length(points[points .> 1])
end



function day5part1(array_vert_hori)
    return part1and2(array_vert_hori)
end

function day5part2(array)
    return part1and2(array)
end

input_day5part1 = deepcopy(array_vert_hori)
input_day5part2 = deepcopy(array)


using BenchmarkTools

@btime day5part1($input_day5part1)
@btime day5part1($input_day5part2)


t1 = time()
num = day5part1(input_day5part1)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

t1 = time()
num = day5part2(input_day5part2)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")

# ==============================================================================
# potential
# ==============================================================================
# create more points at once and append them all at once rather than one by one
# or rewrite for mathematical lines


