# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day14_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
template, rules = split(input, "\n\n")

rules = [split(x, " -> ") for x in split(rules, "\n")]

# ==============================================================================
# build a rules dictionary
# ==============================================================================
# build rules dictionary
rules_dict = Dict()
for (ac, b) in rules
    rules_dict[ac] = b
end

# ==============================================================================
# algorithm part 1 and 2
# ==============================================================================
cache = Dict{String,Dict{SubString{String},Int64}}()
function the_next_generation(str, gen)
    name =  string(str, gen)

    if name in keys(cache)
        return cache[name]

    elseif gen == 1
        result_dict = gen1[str]

    else
        char = rules_dict[str]
        left = str[1] * char
        right = char * str[2]

        result_dict_left = the_next_generation(left, gen - 1)
        result_dict_right = the_next_generation(right, gen - 1)

        result_dict = merge(+, result_dict_left, result_dict_right)

        n = Dict(char => 1)
        result_dict = merge(+, result_dict, n)

        cache[name] = result_dict

        return result_dict
    end
end

function day14part1and2(template, gen)
    res = Dict()
    for i = 1:length(template) - 1
        res_new = the_next_generation(template[i:i + 1], gen)
        own = Dict(string(template[i]) => 1)
        res = merge(+, res, res_new, own)
    end

    res = merge(+, res, Dict(string(template[end]) => 1))
    listt = sort(collect(res), by=x -> x[2], rev=true)
    
    return listt[1][2] - listt[end][2]
end

# build result dictionary
gen1 = Dict{String,Dict{SubString{String},Int64}}()
for (ac, b) in rules
    gen1[ac] = Dict(b => 1)
end


input_day14part1 = [template, 10]
input_day14part2 = [template, 40]

using BenchmarkTools
@btime day14part1and2(input_day14part1...) setup = (empty!(cache))
@btime day14part1and2(input_day14part2...) setup = (empty!(cache))


# ==============================================================================
# with arrays
# ==============================================================================











