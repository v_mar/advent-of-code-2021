# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day10_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = split(input, "\n")

# ==============================================================================
# algo
# ==============================================================================

function day10part1and2(input::Vector{SubString{String}}, part2=false)
    # close same level with the same character
    opens = ["(", "[", "{", "<"]
    closes = [")", "]", "}", ">"]
    op_cl_dict = Dict("(" => ")", "[" => "]", "{" => "}", "<" => ">")

    # reverse the dictionary
    cl_op_dict = Dict(value => key for (key, value) in op_cl_dict)

    scores = Dict(")" => 3, "]" => 57, "}" => 1197, ">" => 25137)

    string_list = []

    error = zeros(Int64, length(input))

    for (index, line) in enumerate(input)
        for charac in line
            if error[index] == 0
                charac = string(charac)
                if charac in opens
                    push!(string_list, charac)
                elseif charac in closes
                    if string_list[end] == cl_op_dict[charac]
                        deleteat!(string_list, length(string_list))
                    else
                        error[index] = scores[charac]
                    end
                end
            end
        end
    end

    if !part2
        return sum(error)
    else
        # discard the corrupted lines
        input = input[error .== 0]

        scores_part2 = Dict(")" => 1, "]" => 2, "}" => 3, ">" => 4)

        # complete the uncomplete ones
        completed_lines = []
        score_list = zeros(Int64, length(input))

        for (index, line) in enumerate(input)
            string_list = []
            for charac in line
                charac = string(charac)
                if charac in opens
                    push!(string_list, charac)
                elseif charac in closes
                    if string_list[end] == cl_op_dict[charac]
                        deleteat!(string_list, length(string_list))
                    end
                end
            end

            push!(completed_lines, line)

            for charac in reverse(string_list)
                right_one = op_cl_dict[charac]
                score_list[index] = score_list[index] * 5 + scores_part2[right_one]
                completed_lines[end] = string(completed_lines[end], right_one)
            end
        end

        score_list = sort(score_list)
        return score_list[trunc(Int64, (length(score_list) + 1) / 2)]
    end
end

day10part1(input::Vector{SubString{String}}) = day10part1and2(input, false)
day10part2(input::Vector{SubString{String}}) = day10part1and2(input, true)

input_day10part1 = deepcopy(input)
input_day10part2 = deepcopy(input)

using BenchmarkTools
@btime day10part1($input_day10part1)
@btime day10part2($input_day10part2)


t1 = time()
num = day10part1(input_day10part1)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

t1 = time()
num = day10part2(input_day10part2)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")




