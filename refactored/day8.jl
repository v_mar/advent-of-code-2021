# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day8_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = split(input, "\n")

# ==============================================================================
# part 1 and 2
# ==============================================================================
function day8part1(input::Vector{SubString{String}})
    part_one_input = [split(x, " | ")[2] for x in input] 
    part_one_input = [split(x, " ") for x in part_one_input]
    part_one_input = reduce(vcat, part_one_input)

    sig = [4, 2, 7, 3]

    num_instances = 0
    for str in part_one_input
        if length(str) in sig
            num_instances += 1
        end
    end
    return num_instances
end


function prepare_for_part_2(input::Vector{SubString{String}})
    part_two_input = [split(in_, " ") for in_ in input]
    part_two_input = [deleteat!(in_, 11) for in_ in part_two_input]
    return [[sort(collect(i)) for i in inn] for inn in part_two_input]
end


function day8part2(input::Vector{SubString{String}})
    part_two_input = prepare_for_part_2(input)

    summ = 0
    dict_ = Dict()
    inn2 = []

    for inn in part_two_input
        # find the known ones
        for seq = inn
            if length(seq) == 2  dict_[1] = seq
            elseif length(seq) == 3  dict_[7] = seq
            elseif length(seq) == 4  dict_[4] = seq
            elseif length(seq) == 7  dict_[8] = seq
            else  push!(inn2, seq)
            end
        end

        # find the unknown ones
        for seq = inn2
            if length(seq) == 6 # is a 6 or a 9 or a 0
                if (length(intersect(seq, dict_[1])) == 1)  dict_[6] = seq
                elseif (length(intersect(seq, dict_[4])) == 4)  dict_[9] = seq
                elseif (length(intersect(seq, dict_[4])) == 3)  dict_[0] = seq
                end
            elseif length(seq) == 5 # is a 2, a 3, or a 5
                if (length(intersect(seq, dict_[1])) == 2)  dict_[3] = seq
                elseif (length(intersect(seq, dict_[4])) == 2)  dict_[2] = seq
                elseif (length(intersect(seq, dict_[4])) == 3)  dict_[5] = seq
                end
            end
        end

        # translate -> reverse dictionary
        babel_dict = Dict(value => key for (key, value) in dict_)
        translated = [babel_dict[seq] for seq in inn]

        summ += parse(Int64, join(string(translated[11], translated[12], translated[13], translated[14])))

        empty!(dict_)
        empty!(inn2)

    end
    return summ
end

input_day8part1 = deepcopy(input)
input_day8part2 = deepcopy(input)

using BenchmarkTools
@btime day8part1($input_day8part1)
@btime day8part2($input_day8part2)


t1 = time()
num = day8part1(input_day8part1)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

t1 = time()
num = day8part2(input_day8part1)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")

