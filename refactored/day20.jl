# ==============================================================================
# input
# ==============================================================================
# final input
f = open("./inputs/day20_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
# parse
img_enh_alg_, img = split(input, "\n\n")

# this line only for trial input relevant
img_enh_alg_ = replace(img_enh_alg_, "\n" => "")

img_enh_alg = tuple([x == '.' ? 0 : 1 for x in img_enh_alg_]...)
img = [[x == '.' ? 0 : 1 for x in row] for row in split(img, "\n")]
img = copy(reduce(hcat, img)') # -> transpose here

# ==============================================================================
# next attemt
# ==============================================================================

function do_stuff(img, img_enh_alg; padding_size=50, times=1)
    dij = ((-1, -1, -1,  0, 0, 0,  1, 1, 1), 
           (-1,  0,  1, -1, 0, 1, -1, 0, 1))

    pow = (256, 128, 64, 32, 16, 8, 4, 2, 1)

    enlarged = zeros(Int64, (size(img)[1] + padding_size * 2, size(img)[2] + padding_size * 2))
    enlarged[padding_size + 1:end - padding_size, padding_size + 1:end - padding_size] = img
    n_cols, n_rows = size(enlarged)
    images = [enlarged, deepcopy(enlarged)]

    ind_old = 1
    ind_new = 1
    num = 0

    for ll = 1:times * 2
        ind_old = (ll - 1) % 2 + 1
        ind_new = 3 - ind_old

        for i in 1:n_rows, j in 1:n_cols
            num -= num
            for kk = 1:9
                ii = i + dij[1][kk]
                jj = j + dij[2][kk]

                if !(1 <= ii <= n_cols)
                    ii = mod1(ii, n_cols)
                end

                if !(1 <= jj <= n_rows)
                    jj = mod1(jj, n_rows)
                end

                pix = images[ind_old][ii, jj]
                num += pix * pow[kk]
            end

            new_val = img_enh_alg[num + 1]
            images[ind_new][i, j] = new_val
        end
    end
    return length(images[ind_new][images[ind_new] .== 1])
end

number_lit = do_stuff(img, img_enh_alg, padding_size=50, times=1)
number_lit = do_stuff(img, img_enh_alg, padding_size=50, times=25)


using BenchmarkTools
@btime do_stuff(img, img_enh_alg, padding_size=50, times=1)
@btime do_stuff(img, img_enh_alg, padding_size=50, times=25)


# using Profile
# using ProfileView

# Profile.clear()
# @profile do_stuff(img, img_enh_alg, padding_size=50, times=25)
# ProfileView.view()
