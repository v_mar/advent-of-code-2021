# ==============================================================================
# input
# ==============================================================================
# final input
f = open("./inputs/day25_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [collect(i) for i in split(input, "\n")]
input = reduce(hcat, input)

# ------------------------------------------------------------------------------
is, js = size(input)
hmm = Array{Char}(undef, js, is)
for i in 1:is, j in 1:js hmm[j, i] = input[i, j] end
stepp = deepcopy(hmm)

# ==============================================================================
# algorithm
# ==============================================================================

function day25part1(stepp)
    stepps = [stepp, deepcopy(stepp)]
    is, js = size(stepp)

    step_num = 0
    changing = true
    while changing
        changing = false

        for i in 1:is, j in 1:js
            if stepps[1][i, j] == '>' && stepps[1][i, mod1((j + 1), js)] == '.'
                stepps[2][i, j] = '.'
                stepps[2][i, mod1((j + 1), js)] = '>'
                changing = true
            end
        end

        stepps[1] = copy(stepps[2])

        for i in 1:is, j in 1:js
            if stepps[1][i, j] == 'v' && stepps[1][mod1((i + 1), is), j] == '.'
                stepps[2][i, j] = '.'
                stepps[2][mod1((i + 1), is), j] = 'v'
                changing = true
            end
        end

        stepps[1] = copy(stepps[2])
        
        step_num += 1
    end
    return step_num
end



out = day25part1(stepp)

using BenchmarkTools

@btime day25part1(stepp)

# using ProfileView
# using Profile

# Profile.clear()
# @profile day25part1(stepp)
# ProfileView.view()


# # ==============================================================================
# # algorithm FLoops
# # ==============================================================================

# function day25part1(stepp)
#     stepps = [stepp, deepcopy(stepp)]
#     is, js = size(stepp)

#     step_num = 0
#     changing = true
#     while changing
#         changing = false

#         Threads.@threads for i in 1:is
#             for j in 1:js
#                 if stepps[1][i, j] == '>' && stepps[1][i, mod1((j + 1), js)] == '.'
#                     stepps[2][i, j] = '.'
#                     stepps[2][i, mod1((j + 1), js)] = '>'
#                     changing = true
#                 end
#             end
#         end

#         stepps[1] = copy(stepps[2])

#         Threads.@threads for i in 1:is
#             for j in 1:js
#                 if stepps[1][i, j] == 'v' && stepps[1][mod1((i + 1), is), j] == '.'
#                     stepps[2][i, j] = '.'
#                     stepps[2][mod1((i + 1), is), j] = 'v'
#                     changing = true
#                 end
#             end
#         end

#         stepps[1] = copy(stepps[2])
        
#         step_num += 1
#     end
#     return step_num
# end


# @time day25part1(stepp)

