# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day17_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
regex = r"-?\d+"
input = Int64[parse(Int64, x.match) for x in eachmatch(myregex, input)]

# ==============================================================================
# part 1 and 2
# ==============================================================================
function day17part1and2(x_tar_min::Int64, x_tar_max::Int64, y_tar_min::Int64, y_tar_max::Int64)
    y_max_global = 0
    log = Set()
    x = 0
    y = 0
    y_max = 0
    v_x = 0
    v_y = 0

    for v_y_init = -200:200
        for v_x_init = 1:x_tar_max

            x -= x
            y -= y
            y_max -= y_max
            v_x -= v_x
            v_y -= v_y
            v_x += v_x_init
            v_y += v_y_init

            while y > y_tar_min && x < x_tar_max
                x += v_x
                y += v_y
                v_x -= sign(v_x)
                v_y -= 1
                y_max = max(y_max, y)

                if x_tar_min <= x <= x_tar_max && y_tar_min <= y <= y_tar_max
                    y_max_global = max(y_max_global, y_max)
                    push!(log, [v_x_init, v_y_init])
                    break

                end
            end
        end
    end

    return y_max_global, length(log)
end

using BenchmarkTools
@btime day17part1and2(input_day17...)

# using Profile
# using ProfileView

# @profile day17part1and2(input_day17...)
# ProfileView.view()