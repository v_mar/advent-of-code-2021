# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day2_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
input = [split(row, " ") for row in split(input, "\n")]
dirs = [x for (x, y) in input]
dists = [parse(Int64, y) for (x, y) in input]

# ==============================================================================
# part1
# ==============================================================================

function day2part1(dirs, dists)
    x = 0
    y = 0

    for ii = 1:length(dists)
        dir = dirs[ii]
        dist = dists[ii]

        if dir == "forward"
            x += dist
        
        elseif dir == "down"
            y += dist
        
        elseif dir == "up"
            y -= dist
        end
    end
    return x * y
end

t1 = time()
num = day2part1(dirs, dists)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day2part1(dirs, dists)

# ==============================================================================
# part 2
# ==============================================================================

function day2part2(dirs, dists)
    x = 0
    y = 0
    aim = 0

    for ii = 1:length(dists)
        dir = dirs[ii]
        dist = dists[ii]

        if dir == "forward"
            y += dist
            x += aim * dist
        
        elseif dir == "down"
            aim += dist
        
        elseif dir == "up"
            aim -= dist
        end
    end
    return x * y
end

t1 = time()
num = day2part2(dirs, dists)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")

using BenchmarkTools
@btime day2part2(dirs, dists)


