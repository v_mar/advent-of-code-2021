using Base:Number

using OffsetArrays
# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day23_input.txt")
input = read(f, String)
close(f)

ins_part2 = "#D#C#B#A#\n#D#B#A#C#"

# ==============================================================================
# parse input -> tedious hard coding
# ==============================================================================
input_ = replace(input, " " => "#")
input_ = [collect(x) for x in split(input_, "\n")]
append!(input_[4], ['#', '#'])
append!(input_[5], ['#', '#'])

input_part1 = join([join(x) * "\n" for x in input_])[1:end - 1]

ins_part2_ = [collect(x) for x in split(ins_part2, "\n")]
insert!(ins_part2_[1], 1, '#')
insert!(ins_part2_[1], 1, '#')
insert!(ins_part2_[2], 1, '#')
insert!(ins_part2_[2], 1, '#')
append!(ins_part2_[1], ['#', '#'])
append!(ins_part2_[2], ['#', '#'])


input__ = vcat(input_[1:3], ins_part2_, input_[end - 1:end])
input_part2 = join([join(x) * "\n" for x in input__])[1:end - 1]

# ==============================================================================
# parse input
# ==============================================================================
function parse_input(input)
    input = [collect(i) for i in split(input, "\n")]
    input = reduce(hcat, input)
    input = input[2:end - 1, 2:end - 1]
    return join(input)
end

input_part1 = parse_input(input_part1)
input_part2 = parse_input(input_part2)

# ==============================================================================
# algorithm
# ==============================================================================
# target state part one
target_part1  = "...........##A#B#C#D####A#B#C#D##"

# target state part two
target_part2 = "...........##A#B#C#D####A#B#C#D####A#B#C#D####A#B#C#D##"

# ------------------------------------------------------------------------------
function swap_chars_at(str, iii1, ind2)
    iii2 = convert(ind2[1], ind2[2])
    inds = minmax(iii1, iii2)
    fii1 = inds[1]
    fii2 = inds[2]

    return str[1:fii1 - 1] * str[fii2] * str[fii1 + 1:fii2 - 1] * str[fii1] * str[fii2 + 1:end]    
end

function find_on_path(cur_state, i_r, j_r; valid_chars=('.'))
    free = true
    for i in i_r, j in j_r
        if !(cur_state[convert(i, j)] in valid_chars)
            free = false
            break
        end
    end
    return free
end

convert(i, j) = (i - 1) * 11 + j
reconvert(iii) = (iii - 1) ÷ 11 + 1, (iii - 1) % 11 + 1


function day23part1and2(input, target)
    states_n_costs = Dict(input => 0)
    cur_cost = 0
    found_min = typemax(Int64)

    queue_length = 15000

    prio_queue = OffsetVector([String[] for _ in 1:queue_length + 1], 0:queue_length)
    push!(prio_queue[cur_cost % queue_length], input)

# ==========================================================================
    step_cost = Dict('A' => 1, 'B' => 10, 'C' => 100, 'D' => 1000)
    dest = Dict('A' => 3, 'B' => 5,  'C' => 7,   'D' => 9)
    j_hallway = (1, 2, 4, 6, 8, 10, 11)

    is, js = trunc(Int64, length(input) / 11), 11

    valid_inds_part1 = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 14, 16, 18, 20, 
                                                            25, 27, 29, 31)

    valid_inds_part2 = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 14, 16, 18, 20, 
                            25, 27, 29, 31, 36, 38, 40, 42, 47, 49, 51, 53)

    valid = is == 3 ? valid_inds_part1 : valid_inds_part2

    while cur_cost - found_min < 1000 # should be queue_length
        for cur_state in prio_queue[cur_cost % queue_length]

            for iii in valid
                cur_char = cur_state[iii]
                if cur_char != '.'
                    inds = reconvert(iii)
                    i = inds[1]; j = inds[2]
                    destin = dest[cur_char]

                    # check if current columns is finished                       
                    if !(destin == j && find_on_path(cur_state, i:is, j, valid_chars=('.', cur_char)))
# ==============================================================================
# move from hallway to tunnel
# ==============================================================================            
                        if i == 1
                                # look if the hallway is free till the destination
                            range = j > destin ? (destin:j - 1) : (j + 1:destin)

                            if find_on_path(cur_state, 1, range)  

                                # only spaces n same chars in dest
                                if find_on_path(cur_state, 2:is, destin, valid_chars=('.', cur_char))

                                    ind = 1
                                    for ii = is:-1:1
                                        if cur_state[convert(ii, destin)] == '.'
                                            ind = ii
                                            break
                                        end
                                    end

                                    next_state = swap_chars_at(cur_state, iii, (ind, destin))
                                    next_cost = cur_cost + step_cost[cur_char] * (abs(destin - j) + ind - 1)

                                    if !(next_state in keys(states_n_costs)) || (states_n_costs[next_state] > next_cost)
                                        states_n_costs[next_state] = next_cost
                                        push!(prio_queue[next_cost % queue_length], next_state)

                                    end
                                end
                            end
# ==============================================================================
# move from tunnel to hallway
# ============================================================================== 
                        elseif i > 1 && find_on_path(cur_state, 1:i - 1, j)
                            for jj in j_hallway
                                range = j > jj ? (jj:j - 1) : (j + 1:jj)

                                if find_on_path(cur_state, 1, range)
                                    next_state = swap_chars_at(cur_state, iii, (1, jj))                                    
                                    next_cost = cur_cost + step_cost[cur_char] * (i - 1 + abs(jj - j))

                                    if !(next_state in keys(states_n_costs)) || (states_n_costs[next_state] > next_cost)
                                        states_n_costs[next_state] = next_cost
                                        push!(prio_queue[next_cost % queue_length], next_state)
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end

        empty!(prio_queue[cur_cost % queue_length])
        cur_cost += 1
        # println(cur_cost)
        # println(states_n_costs)

        if target in keys(states_n_costs)
            found_at = states_n_costs[target]
            found_min = min(found_min, found_at)
        end
    end
    return states_n_costs[target]
end


res = day23part1and2(input_part1, target_part1)
res = day23part1and2(input_part2, target_part2)


using BenchmarkTools

@btime day23part1and2($input_part1, $target_part1)   
@btime day23part1and2($input_part2, $target_part2)



# using Profile
# using ProfileView

# Profile.clear()
# @profile day23part1and2(input_part1, target_part1)
# ProfileView.view()

