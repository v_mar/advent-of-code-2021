using Base:Bool
using OffsetArrays

# ==============================================================================
# read input
# ==============================================================================
f = open("./inputs/day15_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input and build complete field
# ==============================================================================
input = [parse.(Int8, collect(x)) for x in split(input, "\n")]
input = reduce(hcat, input)
input_day15part1 = deepcopy(input)

# ==============================================================================
# build complete field
# ==============================================================================
tmp = vcat((mod1.(input .+ Int8(i), Int8(9)) for i = 0:4)...)
riskmap = hcat((mod1.(tmp .+ Int8(i), Int8(9)) for i = 0:4)...)
input_day15part2 = deepcopy(riskmap)

# ==============================================================================
# fixed prioity list --> fastest by far :))
# ==============================================================================
function path_finder(search_matrix)
    dij = (CartesianIndex(1, 0), CartesianIndex(-1, 0), CartesianIndex(0, 1), CartesianIndex(0, -1))
    
    lowest_risk = fill(typemax(Int32), size(search_matrix))
    lowest_risk[1,1] = 0
    
    visited = zeros(Bool, size(search_matrix))
    
    next_ones = OffsetVector([CartesianIndex{2}[] for _ in 1:10], 0:9)
    push!(next_ones[0], CartesianIndex(1, 1)) 
    cur_risk = 0
    
    while true
        for curind in next_ones[cur_risk % 10]
            visited[curind] = true   

            for kk = 1:4
                nextind = curind + dij[kk]
                (!(checkbounds(Bool, visited, nextind)) || visited[nextind]) && continue

                next_risk = search_matrix[nextind] + cur_risk
                    
                if next_risk < lowest_risk[nextind]
                    lowest_risk[nextind] = next_risk
                    push!(next_ones[next_risk % 10], nextind)
                end                  
            end
        end
        visited[end,end] && break
        empty!(next_ones[cur_risk % 10])
        cur_risk += 1
    end
    return lowest_risk[end, end]
end

using BenchmarkTools
@btime path_finder(input_day15part1) # 338.041 μs (65 allocations: 70.81 KiB) ->  589
@btime path_finder(input_day15part2) # 8.798 ms (86 allocations: 1.27 MiB)    -> 2885

# # ==============================================================================
# # with A*
# # ==============================================================================
# function heuristic(ind; goal=500)
#     aa = (goal - ind[1] + goal - ind[2]) ÷ 2

#     return aa
# end

# function path_finder(search_matrix)
#     dij = (CartesianIndex(1, 0), CartesianIndex(-1, 0), CartesianIndex(0, 1), CartesianIndex(0, -1))
    
#     lowest_risk = fill(typemax(Int32), size(search_matrix))
#     lowest_risk[1,1] = 0

#     goal_ = size(search_matrix)[1]
    
#     visited = zeros(Bool, size(search_matrix))
    
#     next_ones = OffsetVector([CartesianIndex{2}[] for _ in 1:10], 0:9)
#     push!(next_ones[0], CartesianIndex(1, 1)) 
#     cur_risk = 0
    
#     while true
#         for curind in next_ones[cur_risk % 10]
#             visited[curind] = true   

#             for kk = 1:4
#                 nextind = curind + dij[kk]
#                 (!(checkbounds(Bool, visited, nextind)) || visited[nextind]) && continue

#                 next_risk = search_matrix[nextind] + lowest_risk[curind]
                    
#                 if next_risk < lowest_risk[nextind]
#                     lowest_risk[nextind] = next_risk
#                     push!(next_ones[(next_risk + heuristic(nextind, goal=goal_)) % 10], nextind)
#                 end                  
#             end
#         end
#         lowest_risk[end, end] == 589 && break
#         empty!(next_ones[cur_risk % 10])
#         cur_risk += 1

#     end
#     return lowest_risk[end, end]
# end


# using BenchmarkTools
# @btime path_finder(input_day15part1)
# @btime path_finder(input_day15part2)


# nextind = CartesianIndex(50, 50)