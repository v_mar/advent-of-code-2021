# ==============================================================================
# input
# ==============================================================================
f = open("./inputs/day7_input.txt")
input = read(f, String)
close(f)

# ==============================================================================
# parse input
# ==============================================================================
vector = parse.(Int64, split(input, ","))

# using StaticArrays
# vector = SizedMatrix{1000,1}(vector)

# ==============================================================================
# part 1
# ==============================================================================

function day7part1(vector::Vector{Int64})
    min_fuel = typemax(Int64)

    fuel_req = 0
    for pos = minimum(vector):maximum(vector)  # make the region smaller
        fuel_req -= fuel_req
        for i = 1:length(vector)
            fuel_req += abs(vector[i] - pos)
        end
    
        if fuel_req < min_fuel
            min_fuel = fuel_req
        else
            break           # added a break when the next value is increasing, risky?
        end
    end
    return min_fuel
end

function day7part2(vector::Vector{Int64})
    min_fuel = typemax(Int64)

    fuel_req = 0
    for pos = minimum(vector):maximum(vector) # make the region smaller
        fuel_req -= fuel_req
        for i = 1:length(vector)
            for ii = 1:abs(vector[i] - pos)
                fuel_req += ii
            end
        end

        if fuel_req < min_fuel
            min_fuel = fuel_req
        else
            break           # added a break when the next value is increasing, risky?
        end
    end
    return min_fuel
end

input_day7part1 = deepcopy(vector)
input_day7part2 = deepcopy(vector)

using BenchmarkTools
@btime day7part1($input_day7part1)
@btime day7part2($input_day7part2)


t1 = time()
num = day7part1(input_day7part1)
println("part 1: ", num, " | solved in ", (time() - t1) * 1000, " ms")
# @btime day7part1(vector)

t1 = time()
num = day7part2(input_day7part2)
println("part 2: ", num, " | solved in ", (time() - t1) * 1000, " ms")
# @btime day7part2(vector)

# ==============================================================================
# potential
# ==============================================================================
# write a iterative solution/solver jumping left and right?
