# Advent of Code 2021

This was my first AoC and I really enjoyed the puzzles, the story, and, last but not least, the subreddit. I learned a lot about data structures and algorithms, since I don't come from a CS background and had learned to code (mostly) on my own. Knowing only Python, I decided to learn Julia during AoC. I really struggled with some of the puzzles, but continued obsessively (sometimes for days) until I finished all of them. Luckily, I had a lot of free time during December. 

After finishing, I decided to refactor and optimize the code as good as I can to better understand how to get the performance benefits which Julia advertises. This took another month. For the refactoring, I went through some solutions in the Solution Megathreads to get inspiration or to more or less copy/translate and understand the solutions. 

With the help of some solutions takes from reddit, all the puzzles finish in under 300 ms :) . The timings reflect only the algorithm and not the read-in or the pre processing. (Macbook Air M1; Julia 1.7)

Any suggestions to further increase the performance (without too much hardcoding) or the readability are appreciated :)

## Timings of Advent of Code 2021

| Day   | help for init | help for opti | part 1 | part 2 |
| --- | --- | --- | --- | --- |
| day 1 | ----|---- | **1.258 μs** (1 allocation: 16 bytes) | **1.883 μs** (1 allocation: 16 bytes) |
| day 2 | ----|---- | **5.819 μs** (1 allocation: 16 bytes) | **5.798 μs** (1 allocation: 16 bytes) |
| day 3 | ----|---- | **5.514 μs** (9 allocations: 576 bytes) | **44.417 μs** (114 allocations: 91.62 KiB) |
| day 4 | ----|---- | **136.000 μs** (408 allocations: 43.52 KiB) | **400.958 μs** (411 allocations: 45.38 KiB) |
| day 5 | *---|---- | **322.709 μs** (683 allocations: 1.94 MiB) | **544.667 μs** (1008 allocations: 3.18 MiB) |
| day 6 | ----|---- | **2.259 μs** (81 allocations: 10.12 KiB) | **6.392 μs** (257 allocations: 32.12 KiB) |
| day 7 | ----|---- | **47.708 μs** (0 allocations: 0 bytes) | **666.416 μs** (0 allocations: 0 bytes) |
| day 8 | ----|---- | **270.875 μs** (5206 allocations: 370.98 KiB) | **2.871 ms** (62835 allocations: 4.02 MiB) |
| day 9 | ----|---- | **56.041 μs** (2 allocations: 208 bytes) | **847.458 μs** (19 allocations: 106.02 KiB) |
| day 10| ----|---- | **292.584 μs** (7282 allocations: 251.22 KiB) | **524.167 μs** (13038 allocations: 521.02 KiB) |
| day 11| ----|---- | **61.958 μs** (4 allocations: 1.36 KiB) | **212.667 μs** (534 allocations: 34.48 KiB) |
| day 12| ----|*--- | **847.279 ns** (7 allocations: 421 bytes) | **3.237 μs** (26 allocations: 1.33 KiB) | 
| day 13| ----|---- | **60.083 μs** (1663 allocations: 123.58 KiB) | **316.792 μs** (5359 allocations: 1.00 MiB) |
| day 14| ----|---- | **844.875 μs** (21240 allocations: 1.88 MiB) | **4.579 ms** (114378 allocations: 10.27 MiB) |
| day 15| *---|*--- | **265.209 μs** (40 allocations: 71.84 KiB) | **7.273 ms** (56 allocations: 1.34 MiB) |
| day 16| ----|---- | **1.525 ms** (8463 allocations: 1.01 MiB) | **19.833 μs** (310 allocations: 15.97 KiB) |
| day 17| ----|---- | **1.382 ms** (10449 allocations: 576.77 KiB) | solved in pt. 1 |
| day 18| *---|---- | **493.500 μs** (495 allocations: 221.67 KiB) | **12.215 ms** (76460 allocations: 16.47 MiB) |
| day 19| ----|*--- | **5.391 ms** (73186 allocations: 6.78 MiB) | **108.125 μs** (1980 allocations: 127.62 KiB) | 
| day 20| ----|---- | **1.025 ms** (13 allocations: 673.34 KiB)	 | **24.658 ms** (13 allocations: 764.59 KiB) |
| day 21| ----|***- | **1.592 μs** (2 allocations: 96 bytes) | **102.667 μs** (150 allocations: 94.53 KiB) |
| day 22| ----|***- | **5.066 ms** (148915 allocations: 22.98 MiB) | **12.991 ms** (293390 allocations: 44.22 MiB) |
| day 23| ----|*--- | **63.535 ms** (926419 allocations: 45.55 MiB) | **74.195 ms** (733372 allocations: 43.24 MiB) |
| day 24| **--|**** | **22.019 ms** (262 allocations: 11.80 KiB) | solved in pt. 1 |
| day 25| ----|---- | **37.202 ms** (1445 allocations: 52.44 MiB) | no pt. 2 |

Final sum of execution times is 282.6 ms :))

## Whose help?

- Day 12 -> inspiration by reddit/tuisto_mannus to use primes and memoization
- Day 15 -> inspiration to iterate over a list of lists instead of a priority queue
- Day 19 -> inspiration by reddit/prafster to use only two scanners to verify the valid orientation
- Day 21 -> translated the algorithm from reddit/i_have_no_biscuits 's solution more or less completely for the refactored version
- Day 22 -> translated the algorithm from reddit/codepoetics more or less completely for the refactored version
- Day 23 -> inspiration by reddit/nightcracker that moving from tunnel to tunnel is redundant
- Day 24 -> copied the algorithm from reddit/aexl more or less completely for the refactored version